# music_player_demo

other things to look into:

- https://pub.dev/packages/audio_service - lock-screen/background audio management


things I had to do for linux desktop:
 
- update cmake
- install ninja
- update c++ standard library (gcc-9 and g++-9)
- use `flutter build linux` instead of `flutter run -d linux`
- install gstreamer1.0-plugins-bad
