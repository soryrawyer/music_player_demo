// an audio player screen
// includes a "play" button that, when pressed, will play music

import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:just_audio/just_audio.dart';
import 'package:just_audio_platform_interface/just_audio_platform_interface.dart';

// can't use localhost because android has a special IP for local stuff
// const String host = "10.0.2.2";
const String host = "localhost";
const int port = 3000;
const String scheme = "http";

class Player extends StatefulWidget {
  @override
  _PlayerState createState() => _PlayerState();
}

// maybe for now, we can have everything in one widget and break things out
// once there's a good sense of what the structure might be
class _PlayerState extends State<Player> {
  late Future<List<Track>> futureTracks;
  Future<String>? futureTrackUrl;
  late AudioPlayer _audioPlayer;
  final _saved = <Track>{}; // a set of saved Tracks

  @override
  void initState() {
    super.initState();
    _audioPlayer = AudioPlayer();
    futureTracks = fetchTracks();
  }

  @override
  void dispose() {
    _audioPlayer.dispose().catchError((error) {
      stderr.write('dispose');
      stderr.write(error);
    });
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('track page'),
      ),
      // include two rows: one for song titles, another for the player itself
      // when a song is pressed, update the audio player source
      body: Column(children: [
        _trackList(),
        const Divider(),
        // future builder, waiting on track url?
        playerButtonOrWaiting()
      ]),
    );
  }

  // TODO: pull this out into its own widget!
  Widget playerButtonOrWaiting() {
    if (futureTrackUrl == null) {
      return const CircularProgressIndicator();
    } else {
      return FutureBuilder<String>(
          future: futureTrackUrl,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              // audioplayer set uri
              final uri = Uri.parse(snapshot.data!);
              _audioPlayer
                  .setAudioSource(AudioSource.uri(uri))
                  .catchError((error) {
                stderr.write('set audio source');
                stderr.writeln(error);
              });
              return _buildPlayerButtons();
            } else if (snapshot.hasError) {
              return Text("${snapshot.error}");
            } else {
              return const CircularProgressIndicator();
            }
          });
    }
  }

  Widget _buildPlayerButtons() {
    return Center(
      child: StreamBuilder<PlayerState>(
          stream: _audioPlayer.playerStateStream,
          builder: (context, snapshot) {
            final playerState = snapshot.data;
            return _playerButton(playerState);
          }),
    );
  }

  // todo: include a button to save files. it should initiate a download
  // to the local filesystem and also include the track in a "local tracks"
  // state object (maybe eventually this'll be a sqlite table or something?)
  Widget _trackList() {
    return Flexible(
      child: FutureBuilder<List<Track>>(
          future: futureTracks,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return ListView(
                children: _buildListView(snapshot.data!),
              );
            } else if (snapshot.hasError) {
              return Text("${snapshot.error}");
            } else {
              return const CircularProgressIndicator();
            }
          }),
    );
  }

  List<Widget> _buildListView(List<Track> tracks) {
    return tracks.map(_trackRow).toList();
  }

  Widget _trackRow(Track track) {
    final alreadySaved = _saved.contains(track);
    return ListTile(
        title:
            Text("${track.artistName} - ${utf8.decode(track.title.codeUnits)}"),
        trailing: IconButton(
            icon: alreadySaved
                ? const Icon(Icons.add_box_rounded)
                : const Icon(Icons.add_box_outlined),
            color: alreadySaved ? Colors.lightGreen : null,
            tooltip: alreadySaved ? "remove from downloaded" : "download",
            onPressed: () {
              setState(() {
                if (alreadySaved) {
                  _saved.remove(track);
                } else {
                  _saved.add(track);
                }
              });
            }),
        onTap: () {
          // maybe a Provider would be better for when this is in a different
          // class as the play button. or maybe we already need a provider? idk
          setState(() {
            futureTrackUrl = fetchTrackUrl(track.id);
          });
        });
  }

  Widget _playerButton(PlayerState? playerState) {
    final processingState = playerState?.processingState;
    if (processingState == ProcessingState.loading ||
        processingState == ProcessingState.buffering) {
      // if we're currently loading or buffering, return a progress indicator
      print('my code');
      print(processingState);
      return Container(
        margin: const EdgeInsets.all(8.0),
        width: 64.0,
        height: 64.0,
        child: const CircularProgressIndicator(),
      );
    } else if (!_audioPlayer.playing) {
      // we're not currently playing, so return a play button
      return IconButton(
        icon: const Icon(Icons.play_arrow),
        iconSize: 64.0,
        onPressed: _audioPlayer.play,
      );
    } else if (processingState != ProcessingState.completed) {
      // we're in the middle of a track, so offer the option to pause
      return IconButton(
        icon: const Icon(Icons.pause),
        iconSize: 64.0,
        onPressed: _audioPlayer.pause,
      );
    } else {
      return IconButton(
        icon: const Icon(Icons.replay),
        iconSize: 64.0,
        onPressed: () => _audioPlayer.seek(Duration.zero,
            index: _audioPlayer.effectiveIndices?.first),
      );
    }
  }

  Future<List<Track>> fetchTracks() async {
    final uri = Uri(
        scheme: scheme,
        host: host,
        path: "/tracks",
        port: port,
        queryParameters: {"artist_name": "toe"});
    final response = await http.get(uri);
    List<dynamic> trackListJson = jsonDecode(response.body)['results'];
    return trackListJson.map((track) => Track.fromJson(track)).toList();
  }

  Future<String> fetchTrackUrl(String trackId) async {
    final uri = Uri(
        scheme: scheme,
        host: host,
        port: port,
        path: "/download_link/$trackId");
    final response = await http.get(uri);
    return jsonDecode(response.body)["url"];
  }
}

class Track {
  final String id;
  final String artistName;
  final String albumName;
  final String title;

  const Track(
      {required this.id,
      required this.artistName,
      required this.albumName,
      required this.title});

  factory Track.fromJson(Map<String, dynamic> json) {
    return Track(
      id: json['id'],
      artistName: json['artist_name'],
      albumName: json['album_name'],
      title: json['title'],
    );
  }
}

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text('music player')),
        body: Center(
          child: ElevatedButton(
            child: const Text('show tracks'),
            onPressed: () {
              // navigate to the tracks screen
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => Player()));
            },
          ),
        ));
  }
}
