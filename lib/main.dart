import 'package:flutter/material.dart';
import 'package:music_player_demo/screens/player.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    print('hello, building the app now');
    return MaterialApp(
      title: 'just_audio Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      // home: Player(),
      home:
          const HomeScreen(), // change this to be a container with the home screen and the audio player widget
    );
  }
}
